package ru.evgeny.kolesnikov;

import ru.evgeny.kolesnikov.gui.*;
import ru.evgeny.kolesnikov.move.MoveElevaorQueue;
import ru.evgeny.kolesnikov.move.MoveStandart;

import javax.swing.*;

import static org.junit.Assert.assertEquals;

public class Test {
    @org.junit.Test
    public void testElevatorQueue(){
        Elevator elevator = new Elevator(new MoveElevaorQueue());
        elevator.setLevel(2);
        assertEquals("Не верный этаж",elevator.getLevel(),2);
        elevator.setLevel(100);
        assertEquals("Не верный этаж",elevator.getLevel(),10);
        elevator.setLevel(-10);
        assertEquals("Не верный этаж",elevator.getLevel(),1);
        elevator.getMoveAction().addLevel(3);
        elevator.getMoveAction().addLevel(10);
        elevator.getMoveAction().addLevel(7);
        elevator.getMoveAction().addLevel(1);
        elevator.performMove(new JSlider());
    }
    @org.junit.Test
    public void testElevatorStandart(){
        Elevator elevator = new Elevator(new MoveStandart());
        elevator.setLevel(2);
        assertEquals("Не верный этаж",elevator.getLevel(),2);
        elevator.setLevel(100);
        assertEquals("Не верный этаж",elevator.getLevel(),10);
        elevator.setLevel(-10);
        assertEquals("Не верный этаж",elevator.getLevel(),1);
        elevator.getMoveAction().addLevel(3,new JSlider());
        elevator.getMoveAction().addLevel(10,new JSlider());
        elevator.getMoveAction().addLevel(7,new JSlider());
        elevator.getMoveAction().addLevel(1,new JSlider());
        elevator.performMove(new JSlider());
    }

    // отключен т.к мешает автосборке на bitbuketK
//    @org.junit.Test
    public void GuiTest(){
        ElevatorGui elevatorGui = new ElevatorGui();
    }
}
