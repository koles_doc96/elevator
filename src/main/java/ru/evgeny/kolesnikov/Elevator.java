/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov;

import ru.evgeny.kolesnikov.move.IMove;

import javax.swing.*;

import static ru.evgeny.kolesnikov.Constant.COUNT_LEVEL;
import static ru.evgeny.kolesnikov.Constant.MIN_LEVEL;

public class Elevator {
    private final int countLevel;
    private int level;
    private IMove moveAction;

    public Elevator(IMove moveAction) {
        this.countLevel = COUNT_LEVEL;
        this.level = MIN_LEVEL;
        this.moveAction = moveAction;
    }

    public int getLevel() {
        return level;
    }


    public void setLevel(int level) {
        if(level <= 0) {
            this.level = MIN_LEVEL;
        } else if(level > COUNT_LEVEL){
            this.level = COUNT_LEVEL;
        } else {
            this.level = level;
        }
    }

    public IMove getMoveAction() {
        return moveAction;
    }

    public void setMoveAction(IMove moveAction) {
        this.moveAction = moveAction;
    }

    public void performMove(JSlider jSlider){
        moveAction.move(this, jSlider);
    }

    public int getCountLevel() {
        return countLevel;
    }
}
