package ru.evgeny.kolesnikov.move;

import ru.evgeny.kolesnikov.Elevator;
import ru.evgeny.kolesnikov.state.MoveDownState;
import ru.evgeny.kolesnikov.state.MoveUpState;
import ru.evgeny.kolesnikov.state.Travel;

import javax.swing.*;
import java.util.ArrayDeque;
import java.util.Arrays;

import static ru.evgeny.kolesnikov.Constant.STATE_TIME;

public class MoveStandart implements IMove {
    private ArrayDeque<Integer> queue;
    private Travel travel;
    private int nextLevel;

    public MoveStandart() {
        this.queue = new ArrayDeque<>();
        this.travel = new Travel();
    }

    @Override
    public void move(Elevator elevator, JSlider slider) {
        while (!queue.isEmpty()) {
            nextLevel = queue.pop();
            while (elevator.getLevel() != nextLevel) {
                Travel.getLog().info(Arrays.toString(queue.toArray()));
                travel.execute(elevator, nextLevel);
                slider.setValue(elevator.getLevel());
                try {
                    Thread.sleep(STATE_TIME);
                } catch (InterruptedException e) {
                    Travel.getLog().info(e.getMessage());
                }
            }

        }
    }

    @Override
    public void addLevel(int level, JSlider slider) {
        if (!queue.isEmpty()) {
            int levelEl = Integer.parseInt(String.valueOf(slider.getValue()));
            if (travel.getCurrentIState().getClass().equals(MoveUpState.class) && levelEl < level
                    || travel.getCurrentIState().getClass().equals(MoveDownState.class) &&  levelEl > level) {
                nextLevel = level;
                return;
            }
        }
        queue.add(level);

    }

    @Override
    public void addLevel(int level) {
    }
}
