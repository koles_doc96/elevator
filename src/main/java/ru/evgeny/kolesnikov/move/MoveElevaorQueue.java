/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov.move;

import ru.evgeny.kolesnikov.Elevator;
import ru.evgeny.kolesnikov.state.Travel;

import javax.swing.*;
import java.util.ArrayDeque;

import static ru.evgeny.kolesnikov.Constant.STATE_TIME;

public class MoveElevaorQueue implements IMove {

    private ArrayDeque<Integer> queue;
    private Travel travel;

    public MoveElevaorQueue() {
        this.queue = new ArrayDeque<>();
        this.travel = new Travel();
    }

    @Override
    public void move(Elevator elevator, JSlider slider) {
        while (!queue.isEmpty()) {
            int nextLevel = queue.pop();
            while (elevator.getLevel() != nextLevel) {
                travel.execute(elevator, nextLevel);
                slider.setValue(elevator.getLevel());
                try {
                    Thread.sleep(STATE_TIME);
                } catch (InterruptedException e) {
                    Travel.getLog().info(e.getMessage());
                }
            }
        }
    }

    @Override
    public void addLevel(int level, JSlider slider) {
    }

    @Override
    public void addLevel(int level) {
        queue.add(level);
    }

    public ArrayDeque<Integer> getQueue() {
        return queue;
    }
}
