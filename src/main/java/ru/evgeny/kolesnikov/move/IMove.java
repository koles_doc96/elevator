/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov.move;

import ru.evgeny.kolesnikov.Elevator;

import javax.swing.*;

public interface IMove {
    /**
     * Логика отображения лифта на экране
     * @param elevator текущий лифт
     * @param slider отображение в GUI
     */
    void move(Elevator elevator, JSlider slider);

    /**
     * Добавление этажа
     * @param level на какой этаж отправляем лифт
     * @param slider проверка состояния в GUI
     */
    void addLevel(int level, JSlider slider);

    /**
     * Добавление этажа
     * @param level на какой этаж отправляем лифт
     */
    void addLevel(int level);
}
