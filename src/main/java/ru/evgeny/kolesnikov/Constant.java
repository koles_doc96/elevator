/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov;

public class Constant {
    public static final int COUNT_LEVEL = 10;
    public static final int MIN_LEVEL = 1;
    public static final long STOP_TIME = 2000;
    public static final long STATE_TIME = 1500;
    public static final String PEOPLES = "People";
    public static final String CARGO = "Cargo";
    public static final String ELEVATORS = "Elevators";

    private Constant() {
    }
}
