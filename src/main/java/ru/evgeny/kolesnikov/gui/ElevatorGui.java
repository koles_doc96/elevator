package ru.evgeny.kolesnikov.gui;

import ru.evgeny.kolesnikov.Elevator;
import ru.evgeny.kolesnikov.move.MoveElevaorQueue;
import ru.evgeny.kolesnikov.move.MoveStandart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Dictionary;
import java.util.Hashtable;

import static ru.evgeny.kolesnikov.Constant.*;

public class ElevatorGui extends JFrame {
    private JSlider peopleElevator;
    private JSlider cargoElevatorOnePiece;
    private JSpinner spinner;
    private JSpinner spinnerCargo;
    private Elevator peopleEl;
    private Elevator cargo;

    public ElevatorGui() {
        super(ELEVATORS);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        peopleEl = new Elevator(new MoveElevaorQueue());
        cargo = new Elevator(new MoveStandart());

        BoundedRangeModel modelCargo = new DefaultBoundedRangeModel(MIN_LEVEL, 0, MIN_LEVEL, COUNT_LEVEL);

        spinner = new JSpinner(new SpinnerNumberModel(MIN_LEVEL, MIN_LEVEL, peopleEl.getCountLevel(), MIN_LEVEL));
        spinner.setValue(MIN_LEVEL);
        spinnerCargo = new JSpinner(new SpinnerNumberModel(MIN_LEVEL, MIN_LEVEL, cargo.getCountLevel(), MIN_LEVEL));
        spinnerCargo.setValue(MIN_LEVEL);

        statrTimers();

        // Создание лифтов
        cargoElevatorOnePiece = new JSlider(modelCargo);
        cargoElevatorOnePiece.setEnabled(false);
        cargoElevatorOnePiece.setOrientation(JSlider.VERTICAL);
        cargoElevatorOnePiece.setLabelTable(getIntegerJLabelDictionary());
        cargoElevatorOnePiece.setPaintLabels(true);

        JSlider cargoElevatorTwoPiece = new JSlider(modelCargo);
        cargoElevatorTwoPiece.setEnabled(false);
        cargoElevatorTwoPiece.setOrientation(JSlider.VERTICAL);
        cargoElevatorTwoPiece.setLabelTable(getIntegerJLabelDictionary());
        cargoElevatorTwoPiece.setPaintLabels(true);

        peopleElevator = new JSlider(new DefaultBoundedRangeModel(MIN_LEVEL, 0, MIN_LEVEL, COUNT_LEVEL));
        peopleElevator.setEnabled(false);
        peopleElevator.setLabelTable(getIntegerJLabelDictionary());
        peopleElevator.setOrientation(JSlider.VERTICAL);
        peopleElevator.setPaintLabels(true);

        JButton buttonUp = new JButton("UP");
        buttonUp.addActionListener(e -> {
            int level = Integer.parseInt(String.valueOf(spinner.getValue()));
            peopleEl.getMoveAction().addLevel(level);
            peopleEl.getMoveAction().addLevel(COUNT_LEVEL);
        });

        JButton buttonDown = new JButton("DOWN");
        buttonDown.addActionListener(e -> {
            int level = Integer.parseInt(String.valueOf(spinner.getValue()));
            peopleEl.getMoveAction().addLevel(level);
            peopleEl.getMoveAction().addLevel(MIN_LEVEL);
        });

        JButton buttonUpCargo = new JButton("UP");
        buttonUpCargo.addActionListener(e -> {
            int level = Integer.parseInt(String.valueOf(spinnerCargo.getValue()));
            cargo.getMoveAction().addLevel(level, cargoElevatorTwoPiece);
            cargo.getMoveAction().addLevel(COUNT_LEVEL, cargoElevatorTwoPiece);
        });

        JButton buttonDownCargo = new JButton("DOWN");
        buttonDownCargo.addActionListener(e -> {
            int level = Integer.parseInt(String.valueOf(spinnerCargo.getValue()));
            cargo.getMoveAction().addLevel(level, cargoElevatorTwoPiece);
            cargo.getMoveAction().addLevel(MIN_LEVEL, cargoElevatorTwoPiece);
        });


        JPanel people = new JPanel();
        JPanel cargo = new JPanel();
        cargo.add(new JLabel(CARGO));
        cargo.add(cargoElevatorOnePiece);
        cargo.add(cargoElevatorTwoPiece, BorderLayout.WEST);
        cargo.add(buttonUpCargo, BorderLayout.EAST);
        cargo.add(spinnerCargo, BorderLayout.EAST);
        cargo.add(buttonDownCargo, BorderLayout.EAST);

        JPanel buttons = new JPanel();
        buttons.add(buttonUp, BorderLayout.NORTH);
        buttons.add(spinner, BorderLayout.SOUTH);
        buttons.add(buttonDown, BorderLayout.SOUTH);

        people.add(new JLabel(PEOPLES));
        people.add(peopleElevator, BorderLayout.WEST);
        people.add(buttons, BorderLayout.EAST);
        getContentPane().add(people, BorderLayout.WEST);
        getContentPane().add(cargo, BorderLayout.EAST);

        setSize(700, 300);
        setVisible(true);
    }

    private void statrTimers() {
        Timer peopleTimer = new Timer(1, e -> {
            Thread thr = new Thread(() -> {
                synchronized (peopleEl.getMoveAction()) {
                    peopleEl.performMove(peopleElevator);
                }
            });
            thr.start();
        });
        peopleTimer.start();

        Timer cargoTimer = new Timer(1, e -> {
            Thread thr = new Thread(() -> {
                synchronized (cargo.getMoveAction()) {
                    cargo.performMove(cargoElevatorOnePiece);
                }
            });
            thr.start();
        });

        cargoTimer.start();
    }

    private Dictionary<Integer, JLabel> getIntegerJLabelDictionary() {
        // Таблица с надписями ползунка
        Dictionary<Integer, JLabel> labels = new Hashtable<>();
        labels.put(1, new JLabel("<html><font color=blue size=4>1"));
        labels.put(2, new JLabel("<html><font color=blue size=4>2"));
        labels.put(3, new JLabel("<html><font color=blue size=4>3"));
        labels.put(4, new JLabel("<html><font color=blue size=4>4"));
        labels.put(5, new JLabel("<html><font color=blue size=4>5"));
        labels.put(6, new JLabel("<html><font color=blue size=4>6"));
        labels.put(7, new JLabel("<html><font color=blue size=4>7"));
        labels.put(8, new JLabel("<html><font color=blue size=4>8"));
        labels.put(9, new JLabel("<html><font color=blue size=4>9"));
        labels.put(10, new JLabel("<html><font color=blue size=4>10"));
        return labels;
    }

    public static void main(String[] args) {
        new ElevatorGui();
    }

}
