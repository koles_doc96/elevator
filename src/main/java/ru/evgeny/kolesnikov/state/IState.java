/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov.state;

import ru.evgeny.kolesnikov.Elevator;

public interface IState {
    /**
     * Движение лифта
     * @param elevator лифт
     * @param travel состояние ( едем вверх, едем вниз, стоим)
     * @param level на сколько этажей перемещаемся
     */
    void execute(Elevator elevator, Travel travel, int level);
}
