/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov.state;

import ru.evgeny.kolesnikov.Elevator;

import java.util.logging.Logger;

public class Travel {
    private IState currentIState;
    private static Logger log = Logger.getLogger(Travel.class.getName());
    public Travel() {
        this.currentIState = new StandState();
    }

    public void setCurrentIState(IState currentIState) {
        this.currentIState = currentIState;
    }

    public void execute(Elevator elevator, int level) {
        currentIState.execute(elevator, this, level);
    }

    public static Logger getLog() {
        return log;
    }

    public IState getCurrentIState() {
        return currentIState;
    }
}
