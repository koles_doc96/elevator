package ru.evgeny.kolesnikov.state;

import ru.evgeny.kolesnikov.Elevator;

public class MoveDownState implements IState {
    /**
     * Движение лифта вниз
     * @param elevator лифт
     * @param travel состояние
     * @param level на сколько этажей перемещаемся
     */
    @Override
    public void execute(Elevator elevator, Travel travel, int level) {
        Travel.getLog().info("Previous level");
        elevator.setLevel(elevator.getLevel() - 1);
        if(elevator.getLevel() == level){
            Travel.getLog().info("Stop on the floor");
            travel.setCurrentIState(new StandState());
        }
    }
}
