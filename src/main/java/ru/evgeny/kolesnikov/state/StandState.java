/*
 * Copyright (c) 2019. From Evgeny Kolesnikov
 */

package ru.evgeny.kolesnikov.state;

import ru.evgeny.kolesnikov.Elevator;

import static ru.evgeny.kolesnikov.Constant.STOP_TIME;

public class StandState implements IState {
    /**
     Остановка лифта
     @param elevator лифт
      * @param travel состояние
     * @param level на сколько этажей перемещаемся
     */
    @Override
    public void execute(Elevator elevator, Travel travel, int level) {
        if(elevator.getLevel() < level){
            travel.setCurrentIState(new MoveUpState());
        } else if(elevator.getLevel() > level){
            travel.setCurrentIState(new MoveDownState());
        }
        try {
            Thread.sleep(STOP_TIME);
        } catch (InterruptedException e) {
            Travel.getLog().info(e.getMessage());
        }
    }
}
